# Event Booking App
This is a web app for event booking. A user a signup/login via the platform and create appropriate events on different dates and can also book slot to attend other events.

### Tech Stack
> React, Hooks, Nodejs, Mongodb, Graphql

### Screenshots

![11](https://user-images.githubusercontent.com/33368759/136648685-43bc7d92-27b2-4750-b87a-4d1fdbb0cd8f.PNG)

![22](https://user-images.githubusercontent.com/33368759/136648695-aec78fa9-9a5f-4c22-96a6-07842fff931d.PNG)


![33](https://user-images.githubusercontent.com/33368759/136648699-5ffe7036-43ea-4c49-b41a-ce7091735f3b.PNG)

![44](https://user-images.githubusercontent.com/33368759/136648701-4544e782-bc1b-4d49-a38d-1a56b50a1315.PNG)


 